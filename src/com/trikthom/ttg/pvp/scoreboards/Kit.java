package com.trikthom.ttg.pvp.scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.trikthom.ttg.pvp.API.Lang;
import com.trikthom.ttg.pvp.main.User;

public class Kit {

	public static void setScoreboard(Player player) {
		ScoreboardManager manager = Bukkit.getServer().getScoreboardManager();
	    Scoreboard scoreboard = manager.getNewScoreboard();

	    Objective obj = scoreboard.registerNewObjective("Board", "dummy");
	    obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	    obj.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "      KitPvP      ");
	    
	    Score line1 = obj.getScore(ChatColor.AQUA + "" + ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "------------------");
	    Score levelText = obj.getScore(ChatColor.WHITE + Lang.getTranslation(player, "scoreboard.kit.level") + ":");
	    Score level = obj.getScore(ChatColor.AQUA + "" + ChatColor.GRAY + " " + User.getLevel(player, "kit"));
	    Score spacer1 = obj.getScore(ChatColor.AQUA + " ");
	    Score killsText = obj.getScore(ChatColor.WHITE + Lang.getTranslation(player, "scoreboard.kit.kills") + ":");
	    Score kills = obj.getScore(ChatColor.BLACK + "" + ChatColor.GRAY + " " + User.getKills(player, "kit"));
	    Score spacer2 = obj.getScore(ChatColor.BLACK + " ");
	    Score pointsText = obj.getScore(ChatColor.WHITE + Lang.getTranslation(player, "scoreboard.kit.points") + ":");
	    Score points = obj.getScore(ChatColor.RED + "" + ChatColor.GRAY 	+ " " + User.getPoints(player, "kit"));
	    Score spacer3 = obj.getScore(ChatColor.BLUE + " ");
	    Score website = obj.getScore(ChatColor.RED + "play.trikthom.com");
	    Score line2 = obj.getScore(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "------------------");
	    
	    
	    
	    
	    line1.setScore(12);
	    levelText.setScore(11);
	    level.setScore(10);
	    spacer1.setScore(9);
	    killsText.setScore(8);
	    kills.setScore(7);
	    spacer2.setScore(6);
	    pointsText.setScore(5);
	    points.setScore(4);
	    spacer3.setScore(3);
	    website.setScore(2);
	    line2.setScore(1);
	    
	    player.setScoreboard(scoreboard);
	}

}
