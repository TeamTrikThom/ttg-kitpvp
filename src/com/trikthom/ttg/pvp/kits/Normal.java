package com.trikthom.ttg.pvp.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class Normal {

	public static void getKit(Player p) {
		p.getInventory().clear();
	    p.getInventory().setArmorContents(null);
	    p.setHealth(p.getMaxHealth());
	    List<String> lore = new ArrayList<String>();
	    ItemStack sword = new ItemStack(Material.WOOD_SWORD);
	    ItemMeta swordMeta = sword.getItemMeta();
	    swordMeta.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    swordMeta.setLore(lore);
	    sword.setItemMeta(swordMeta);
	    sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
	    p.getInventory().setItem(0, sword);

	    ItemStack sword2 = new ItemStack(Material.BOW);
	    ItemMeta swordMeta2 = sword2.getItemMeta();
	    swordMeta2.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    swordMeta2.setLore(lore);
	    sword2.setItemMeta(swordMeta2);
	    sword2.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
	    sword2.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
	    p.getInventory().setItem(1, sword2);

	    ItemStack arrow = new ItemStack(Material.ARROW, 32);
	    ItemMeta arrowmeta = arrow.getItemMeta();
	    arrowmeta.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    arrowmeta.setLore(lore);
	    arrow.setItemMeta(arrowmeta);
	    p.getInventory().setItem(7, arrow);

	    ItemStack Helmet = new ItemStack(Material.LEATHER_HELMET);
	    LeatherArmorMeta meta = (LeatherArmorMeta)Helmet.getItemMeta();
	    meta.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    meta.setLore(lore);
	    meta.setColor(Color.TEAL);
	    Helmet.setItemMeta(meta);
	    Helmet.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
	    Helmet.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
	    p.getInventory().setHelmet(Helmet);

	    ItemStack Chest = new ItemStack(Material.LEATHER_CHESTPLATE);
	    LeatherArmorMeta Cmeta = (LeatherArmorMeta)Chest.getItemMeta();
	    Cmeta.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    Cmeta.setLore(lore);
	    Cmeta.setColor(Color.TEAL);
	    Chest.setItemMeta(Cmeta);
	    Chest.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
	    Chest.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
	    Chest.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 1);
	    p.getInventory().setChestplate(Chest);

	    ItemStack Pants = new ItemStack(Material.LEATHER_LEGGINGS);
	    LeatherArmorMeta Lmeta = (LeatherArmorMeta)Pants.getItemMeta();
	    Lmeta.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    Lmeta.setLore(lore);
	    Lmeta.setColor(Color.TEAL);
	    Pants.setItemMeta(Lmeta);
	    Pants.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
	    Pants.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
	    Pants.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 1);
	    p.getInventory().setLeggings(Pants);

	    ItemStack Boots = new ItemStack(Material.LEATHER_BOOTS);
	    LeatherArmorMeta Bmeta = (LeatherArmorMeta)Boots.getItemMeta();
	    Bmeta.setDisplayName(ChatColor.GRAY + "�lSpeler Kit");
	    Bmeta.setLore(lore);
	    Bmeta.setColor(Color.TEAL);
	    Boots.setItemMeta(Bmeta);
	    Boots.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
	    Boots.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
	    p.getInventory().setBoots(Boots);
	    ItemStack apple = new ItemStack(Material.GOLDEN_APPLE);
	    ItemMeta Gapple = apple.getItemMeta();
	    Gapple.setDisplayName(ChatColor.GOLD + "Golden apple");
	    Gapple.setLore(lore);
	    apple.setItemMeta(Gapple);
	    p.getInventory().setItem(2, apple);
	    
	    ItemStack duels = new ItemStack(Material.NETHER_STAR);
	    ItemMeta Cduels = duels.getItemMeta();
	    Cduels.setDisplayName(ChatColor.GOLD + "Duels");
	    duels.setItemMeta(Cduels);
	    p.getInventory().setItem(8, duels);
	    
	    p.updateInventory();
	}

}
