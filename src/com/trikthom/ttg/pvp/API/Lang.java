package com.trikthom.ttg.pvp.API;

import org.bukkit.entity.Player;

import com.trikthom.ttg.pvp.main.Main;

public class Lang {

	public static String getTranslation(Player player, String key) {
		
    	String userLang = Main.pd.getPlayerData().getString(player.getUniqueId() + ".lang");
    	String lang = Main.plugin.getConfig().getString(key);
    	if(userLang.equalsIgnoreCase("nl")) lang = Main.nl.getNLData().getString(key);
    	if(userLang.equalsIgnoreCase("en")) lang = Main.en.getENData().getString(key);
    	
    	return lang;
    	
    }
	
    public static void setLang(Player player, String lang) {
    	
    	Main.pd.getPlayerData().set(player.getUniqueId() + ".lang", String.valueOf(lang));
    	Main.pd.savePlayerData();
    	
    }
}
