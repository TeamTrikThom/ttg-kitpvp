package com.trikthom.ttg.pvp.events;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.trikthom.ttg.pvp.main.Locations;

public class VillagerInteract implements Listener {

	@EventHandler
	public static void onVillagerClick(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		if (entity instanceof CraftVillager) {
			if ((entity.getLocation().getBlock() == Locations.villager1(event.getPlayer()))
					|| (entity.getLocation().getBlock() == Locations.villager2(event.getPlayer()))
					|| (entity.getLocation().getBlock() == Locations.villager3(event.getPlayer()))
					|| (entity.getLocation().getBlock() == Locations.villager4(event.getPlayer()))) {
				event.getPlayer().sendMessage("in aanbouw");
			}
		}
	}
	
}
