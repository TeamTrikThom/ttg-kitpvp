package com.trikthom.ttg.pvp.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.trikthom.ttg.pvp.main.Kits;
import com.trikthom.ttg.pvp.main.Locations;
import com.trikthom.ttg.pvp.main.Scoreboard;
import com.trikthom.ttg.pvp.main.User;

public class Death implements Listener {

	@EventHandler
	public static void onDeath(PlayerDeathEvent event) {
		event.setDeathMessage(null);
		Player player = (Player)event.getEntity();
		if (User.getPoints(player, "kit") > 0) User.removePoints(player, 1, "kit");
		Scoreboard.setScoreboard(player, "kit");
		
	}
	@EventHandler(priority=EventPriority.HIGH)
	public static void onRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		event.setRespawnLocation(Locations.spawn(player));
		Kits.giveKit(player, "normal");
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
    public void PlayerDamageReceive(EntityDamageByEntityEvent event) {
        if(event.getEntity() instanceof Player) {
            Player damaged = (Player) event.getEntity();
            if((damaged.getHealth()-event.getDamage()) <= 0) {
            	Player player = (Player)event.getEntity();
    			Player killer = (Player) event.getDamager();
    			if (User.getGamemode(player).equals("kit") && User.getGamemode(killer).equals("kit")) {
    				if(event.getDamager() instanceof Player) {
    					User.addKill(killer, "kit");
        				User.addPoints(killer, 2, "kit");
        				Scoreboard.setScoreboard(killer, "kit");
    				}
    				if (User.getPoints(player, "kit") > 0) User.removePoints(player, 1, "kit");
    				Scoreboard.setScoreboard(player, "kit");
    			}
            	event.setCancelled(true);
                damaged.teleport(Locations.spawn(damaged));
                damaged.setHealth(20);
                Kits.giveKit(player, "normal");
            }
        }
    }
}
