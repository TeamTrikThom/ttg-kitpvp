package com.trikthom.ttg.pvp.events;

import java.util.Random;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.trikthom.ttg.pvp.main.Locations;
import com.trikthom.ttg.pvp.main.User;

public class Move implements Listener {

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		int px = player.getLocation().getBlockX();
		int pz = player.getLocation().getBlockZ();
		int py = player.getLocation().getBlockY();
		if(px >= -3 && px <= 3 && pz >= 19 && pz <= 21 && py >= 70 && py <= 75 ) {
			Location loc = null;
			switch(new Random().nextInt(5)) {
				case 0:
			  		loc = Locations.kitspawnpoint1(player);
			  		break;
			  	case 1:
					loc = Locations.kitspawnpoint2(player);
			  		break;
			  	case 2:
					loc = Locations.kitspawnpoint3(player);
			  		break;
			  	case 3:
					loc = Locations.kitspawnpoint4(player);
			  		break;
			  	case 4:
					loc = Locations.kitspawnpoint5(player);
			  		break;
			}
			player.getInventory().clear(8);
			player.teleport(loc);
			User.setGamemode(player, "kit");
			player.setGameMode(GameMode.ADVENTURE);
			
		}
	}
	
}
