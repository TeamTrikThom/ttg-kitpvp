package com.trikthom.ttg.pvp.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Quit implements Listener {

	@EventHandler
	public static void onLeave(PlayerQuitEvent event) {
		
		event.setQuitMessage(null);
		
	}
	
}
