package com.trikthom.ttg.pvp.events;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.nametagedit.plugin.NametagEdit;
import com.trikthom.ttg.pvp.API.Lang;
import com.trikthom.ttg.pvp.main.Kits;
import com.trikthom.ttg.pvp.main.Locations;
import com.trikthom.ttg.pvp.main.Main;
import com.trikthom.ttg.pvp.main.Ranks;
import com.trikthom.ttg.pvp.main.User;

public class Join implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		
		Player player = event.getPlayer();
		
		setDefaultPlayerData(player);
		
		event.setJoinMessage(null);
		player.setMaxHealth(20);
		player.setHealth(player.getMaxHealth());
		player.setGameMode(GameMode.ADVENTURE);
		
	    player.teleport(Locations.spawn(player));
	    
	    sendWelcomeMessage(player);
	    
	    User.setGamemode(player, "kit");
	    
	    if (Ranks.isStaff(player)) NametagEdit.getApi().setPrefix(player, Ranks.getRankNametagColor(player) + "");
		else NametagEdit.getApi().setPrefix(player, Ranks.getRankNametagColor(player) + "");
	    
	    Kits.giveKit(player, "normal");
		
	}
	
	public void setDefaultPlayerData(Player player) {
		
		Main.pd.getPlayerData().addDefault(player.getUniqueId() + ".lang", String.valueOf(Main.plugin.getConfig().getString("defaultLang")));
		Main.pd.getPlayerData().addDefault(player.getUniqueId() + ".kit.level", 1);
		Main.pd.getPlayerData().addDefault(player.getUniqueId() + ".kit.kills", 0);
		Main.pd.getPlayerData().addDefault(player.getUniqueId() + ".kit.points", 0);
		Main.pd.getPlayerData().options().copyDefaults(true);
		Main.pd.savePlayerData();
		
	}
	
	public void sendWelcomeMessage(Player player) {
		player.sendMessage(Main.line());
		player.sendMessage(Lang.getTranslation(player, "welcome.1"));
		player.sendMessage("");
		player.sendMessage(Lang.getTranslation(player, "welcome.2"));		
		player.sendMessage(Main.line());
	}
	
}
