package com.trikthom.ttg.pvp.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.trikthom.ttg.pvp.main.Ranks;
import com.trikthom.ttg.pvp.main.User;

import net.md_5.bungee.api.ChatColor;

public class Chat implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String levelPrefix = ChatColor.WHITE + "[" + ChatColor.GRAY + "Level " + User.getLevel(player, "kit") + ChatColor.WHITE + "]";
		event.setCancelled(true);
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(levelPrefix + " " + rankPrefix(p, player) + Ranks.getRankNametagColor(player) + player.getName() + Ranks.getMessageColor(player) + ": " + event.getMessage());
		}
	}
	
	public String rankPrefix(Player sender, Player player) {
		if (Ranks.isStaff(player)) return ChatColor.WHITE + "[" + Ranks.getRankColor(player) + Ranks.getRankPrefix(sender, player) + ChatColor.WHITE + "] ";
		return "";
	}
	
}
