package com.trikthom.ttg.pvp.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;

import com.nametagedit.plugin.NametagEdit;
import com.trikthom.ttg.pvp.main.Ranks;
import com.trikthom.ttg.pvp.main.User;
import com.trikthom.ttg.pvp.menus.Duel;

import ru.tehkode.permissions.events.PermissionEntityEvent;

public class Overall implements Listener {
	
	@EventHandler
	public void food(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onRankChange(PermissionEntityEvent event) {
		Player player = Bukkit.getPlayer(event.getEntity().getName());
		if (event.getAction() == PermissionEntityEvent.Action.INHERITANCE_CHANGED) {
			if (Ranks.isStaff(player)) NametagEdit.getApi().setPrefix(player, Ranks.getRankNametagColor(player) + "" + ChatColor.BOLD + "");
			else NametagEdit.getApi().setPrefix(player, Ranks.getRankNametagColor(player) + "");
		}
	}
	
	@EventHandler
	public void onPlayerUse(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (player.getItemInHand().getType() == Material.NETHER_STAR) {
			if (Ranks.isStaff(player)) Duel.openMainMenu(player);
			else player.sendMessage("�f[�c�lTTG�f] Soon!");
		}
	}
	
	@EventHandler
	public void onHandChange(PlayerItemHeldEvent event) {
		Player player = event.getPlayer();
		ItemStack item = player.getInventory().getItem(event.getNewSlot());
		if (item != null) {
			if (item.getType() == Material.NETHER_STAR) {
				if (Ranks.isStaff(player)) User.setGamemode(player, "duel-lobby");
			}
			else if (User.getGamemode(player).equals("duel-lobby")) User.setGamemode(player, "kit");
		}
		else if (User.getGamemode(player).equals("duel-lobby")) User.setGamemode(player, "kit");
	}
	
	@EventHandler
	public void onClickSlot(InventoryClickEvent event) {
		if (event.getCurrentItem() == null) return;
		if (event.getCurrentItem().getType() == Material.NETHER_STAR) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void PlayerDropItemEvent(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onItemSpawn(ItemSpawnEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE) event.setCancelled(true);
	}
	
	@EventHandler
	public void onbreak(BlockBreakEvent event) {
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE) event.setCancelled(true);
	}
}
