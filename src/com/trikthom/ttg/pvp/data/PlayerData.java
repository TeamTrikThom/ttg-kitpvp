package com.trikthom.ttg.pvp.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;


public class PlayerData {
	  static PlayerData instance = new PlayerData();
	  Plugin p;
	  FileConfiguration playerdata;
	  File pdfile;
	  
	  public static PlayerData getInstance() {
	    return instance;
	  }
	  
	  public void setup(Plugin p) {
	    this.pdfile = new File(p.getDataFolder(), "playerdata.yml");
	    if (!this.pdfile.exists()) {
	      try
	      {
	        this.pdfile.createNewFile();
	        
	      }
	      catch (IOException e)
	      {
	        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand playerdata.yml niet aanmaken!");
	      }
	    }
	    this.playerdata = YamlConfiguration.loadConfiguration(this.pdfile);
	  }
	  
	  public FileConfiguration getPlayerData() {
	    return this.playerdata;
	  }
	  
	  public void savePlayerData() {
	    try
	    {
	      this.playerdata.save(this.pdfile);
	    }
	    catch (IOException e)
	    {
	      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand playerdata.yml niet opslaan!");
	    }
	  }
	  
	  public void reloadPlayerData() {
	    this.playerdata = YamlConfiguration.loadConfiguration(this.pdfile);
	  }
	  
	}
