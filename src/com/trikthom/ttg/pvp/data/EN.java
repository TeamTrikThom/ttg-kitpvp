package com.trikthom.ttg.pvp.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class EN {
	  static EN instance = new EN();
	  Plugin p;
	  FileConfiguration en;
	  File enfile;
	  
	  public static EN getInstance() {
	    return instance;
	  }
	  
	  public void setup(Plugin plugin) {
	    this.enfile = new File(plugin.getDataFolder(), "/lang/EN.yml");
	    if (!this.enfile.exists()) {
	      try
	      {
	        this.enfile.createNewFile();
	        
	      }
	      catch (IOException e)
	      {
	        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand EN.yml niet aanmaken!");
	      }
	    }
	    this.en = YamlConfiguration.loadConfiguration(this.enfile);
	    registerDefaults();
	  }
	  
	  public FileConfiguration getENData() {
	    return this.en;
	  }
	  
	  public void saveENData() {
	    try
	    {
	      this.en.save(this.enfile);
	    }
	    catch (IOException e)
	    {
	      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand EN.yml niet opslaan!");
	    }
	  }
	  
	  public void reloadENData() {
	    this.en = YamlConfiguration.loadConfiguration(this.enfile);
	  }
	  
	  public void registerDefaults() {
		  
		  this.en.addDefault("welcome.1", "        " + ChatColor.AQUA + "Welcome to " + ChatColor.RED + "TrikThom Games");
		  this.en.addDefault("welcome.2", ChatColor.AQUA + "Website: " + ChatColor.RED + "https://play.trikthom.com");
		  
		  this.en.addDefault("staffRanks.owner", "Owner");
		  this.en.addDefault("staffRanks.headdeveloper", "Head Developer");
		  this.en.addDefault("staffRanks.developer", "Developer");
		  this.en.addDefault("staffRanks.admin", "Admin");
		  this.en.addDefault("staffRanks.moderator", "Moderator");
		  this.en.addDefault("staffRanks.builder", "Builder");
		  this.en.addDefault("staffRanks.support", "Support");
		  
		  this.en.addDefault("scoreboard.kit.level", "Level");
		  this.en.addDefault("scoreboard.kit.kills", "Kills");
		  this.en.addDefault("scoreboard.kit.points", "Points");
		  
	  }
	  
}
