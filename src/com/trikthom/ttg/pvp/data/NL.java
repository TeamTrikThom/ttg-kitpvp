package com.trikthom.ttg.pvp.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class NL {
	  static NL instance = new NL();
	  Plugin p;
	  FileConfiguration nl;
	  File nlfile;
	  
	  public static NL getInstance() {
	    return instance;
	  }
	  
	  public void setup(Plugin plugin) {
	    this.nlfile = new File(plugin.getDataFolder(), "/lang/NL.yml");
	    if (!this.nlfile.exists()) {
	      try
	      {
	        this.nlfile.createNewFile();
	        
	      }
	      catch (IOException e)
	      {
	        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand NL.yml niet aanmaken!");
	      }
	    }
	    this.nl = YamlConfiguration.loadConfiguration(this.nlfile);
	    
	    registerDefaults();
	  }
	  
	  public FileConfiguration getNLData() {
	    return this.nl;
	  }
	  
	  public void saveNLData() {
	    try
	    {
	      this.nl.save(this.nlfile);
	    }
	    catch (IOException e)
	    {
	      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand NL.yml niet opslaan!");
	    }
	  }
	  
	  public void reloadNLData() {
	    this.nl = YamlConfiguration.loadConfiguration(this.nlfile);
	  }
	  
	  public void registerDefaults() {
		  
		  this.nl.addDefault("welcome.1", "        " + ChatColor.AQUA + "Welkom op " + ChatColor.RED + "TrikThom Games");
		  this.nl.addDefault("welcome.2", ChatColor.AQUA + "Website: " + ChatColor.RED + "https://play.trikthom.com");
		  
		  this.nl.addDefault("staffRanks.owner", "Eigenaar");
		  this.nl.addDefault("staffRanks.headdeveloper", "Hoofd Developer");
		  this.nl.addDefault("staffRanks.developer", "Developer");
		  this.nl.addDefault("staffRanks.admin", "Admin");
		  this.nl.addDefault("staffRanks.moderator", "Moderator");
		  this.nl.addDefault("staffRanks.builder", "Bouwer");
		  this.nl.addDefault("staffRanks.support", "Helper");
		  
		  this.nl.addDefault("scoreboard.kit.level", "Level");
		  this.nl.addDefault("scoreboard.kit.kills", "Kills");
		  this.nl.addDefault("scoreboard.kit.points", "Punten");
	  }
}
