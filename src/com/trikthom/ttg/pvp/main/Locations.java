package com.trikthom.ttg.pvp.main;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Locations {

	public static Location spawn(Player player) {
		Location loc = new Location(player.getServer().getWorld("world"), 0.5D, 71.5D, 0.5D);
	    loc.setPitch(10.0F);
	    loc.setYaw(0.0F);
		return loc;
	}

	public static Location kitspawnpoint1(Player player) {
		Location loc = new Location(player.getServer().getWorld("world"), 598.5D, 4.0D, 1352.5D);
		loc.setPitch(10.0F);
		loc.setYaw(0.0F);
		return loc;
	}

	public static Location kitspawnpoint2(Player player) {
		Location loc = new Location(player.getServer().getWorld("world"), 483.5D, 10.0D, 1414.5D);
		loc.setPitch(10.0F);
		loc.setYaw(-90.0F);
		return loc;
	}

	public static Location kitspawnpoint3(Player player) {
		Location loc = new Location(player.getServer().getWorld("world"), 605.5D, 8.0D, 1409.5D);
		loc.setPitch(10.0F);
		loc.setYaw(-90.0F);
		return loc;
	}

	public static Location kitspawnpoint4(Player player) {
		Location loc = new Location(player.getServer().getWorld("world"), 608.5D, 2.0D, 1458.5D);
		loc.setPitch(10.0F);
		loc.setYaw(180.0F);
		return loc;
	}

	public static Location kitspawnpoint5(Player player) {
		Location loc = new Location(player.getServer().getWorld("world"), 530.5D, 9.0D, 1376.5D);
		loc.setPitch(10.0F);
		loc.setYaw(90.0F);
		return loc;
	}

	public static Location villager1(Player player) {
		return new Location(player.getServer().getWorld("world"), 7.0D, 70.0D, 7.0D);
	}

	public static Location villager2(Player player) {
		return new Location(player.getServer().getWorld("world"), 7.0D, 70.0D, -7.0D);
	}

	public static Location villager3(Player player) {
		return new Location(player.getServer().getWorld("world"), -7.0D, 70.0D, 7.0D);
	}

	public static Location villager4(Player player) {
		return new Location(player.getServer().getWorld("world"), -7.0D, 70.0D, -7.0D);
	}

}
