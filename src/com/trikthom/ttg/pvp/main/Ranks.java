package com.trikthom.ttg.pvp.main;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.trikthom.ttg.pvp.API.Lang;

public class Ranks {

	static String[] staffRanks = new String[] {
			"owner", "headdeveloper", "developer", "admin", "moderator", "builder", "support"
	};
	
	static ChatColor[] staffRankColors = new ChatColor[] {
			ChatColor.DARK_RED, ChatColor.AQUA, ChatColor.AQUA, ChatColor.RED, ChatColor.BLUE, ChatColor.DARK_GREEN, ChatColor.YELLOW
	};
	
	static ChatColor[] staffRankNametagColors = new ChatColor[] {
			ChatColor.RED, ChatColor.AQUA, ChatColor.AQUA, ChatColor.DARK_AQUA, ChatColor.DARK_AQUA, ChatColor.GREEN, ChatColor.YELLOW
	};
	
	public static ChatColor getMessageColor(Player player) {
		if (isStaff(player)) return ChatColor.WHITE;
		return ChatColor.GRAY;
	}
	
	public static boolean isStaff(Player player) {
		for (String rank : staffRanks) {
			if (Permissions.playerInGroup(player, rank)) return true;
		}
		return false;
	}
	
	public static ChatColor getRankColor(Player player) {
		int i = 0;
		for (String rank : staffRanks) {
			if (Permissions.playerInGroup(player, rank)) return staffRankColors[i];
			i++;
		}
		return ChatColor.GRAY;
	}
	
	public static ChatColor getRankNametagColor(Player player) {
		int i = 0;
		for (String rank : staffRanks) {
			if (Permissions.playerInGroup(player, rank)) return staffRankNametagColors[i];
			i++;
		}
		return ChatColor.GRAY;
	}
	
	public static String getRankPrefix(Player sender, Player player) {
		for (String rank : staffRanks) {
			if (Permissions.playerInGroup(player, rank)) return getRankColor(player) + Lang.getTranslation(sender, "staffRanks." + rank);
		}
		return null;
	}
	
}
