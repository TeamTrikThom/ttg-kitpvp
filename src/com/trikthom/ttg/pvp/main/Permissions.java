package com.trikthom.ttg.pvp.main;

import org.bukkit.OfflinePlayer;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Permissions {

	public static boolean playerInGroup(OfflinePlayer offlinePlayer, String group) {
		PermissionUser user = getUser(offlinePlayer);
		if(user == null) return false;
		return user.inGroup(group);
	}
	
	public static PermissionUser getUser(OfflinePlayer offlinePlayer) {
		return PermissionsEx.getPermissionManager().getUser(offlinePlayer.getUniqueId());
	}
	
}
