package com.trikthom.ttg.pvp.main;

import org.bukkit.entity.Player;

import com.trikthom.ttg.pvp.data.PlayerData;

public class User {

	private static PlayerData pd = PlayerData.getInstance();
	
	public static int getLevel(Player player, String gamemode) {
		return pd.getPlayerData().getInt(player.getUniqueId() + "." + gamemode + ".level");
	}
	
	public static int getKills(Player player, String gamemode) {
		return pd.getPlayerData().getInt(player.getUniqueId() + "." + gamemode + ".kills");
	}
	
	public static int getPoints(Player player, String gamemode) {
		return pd.getPlayerData().getInt(player.getUniqueId() + "." + gamemode + ".points");
	}
	
	public static void addPoints(Player player, int points, String gamemode) {
		pd.getPlayerData().set(player.getUniqueId() + "." + gamemode +".points", getPoints(player, gamemode) + points);
	    pd.savePlayerData();
	    if (getLevel(player, gamemode) * getLevel(player, gamemode) <= getPoints(player, gamemode)) setLevel(player, gamemode);
	}
	
	public static void setLevel(Player player, String gamemode) {
		if ((int)Math.sqrt(getPoints(player, gamemode)) != 0) pd.getPlayerData().set(player.getUniqueId() + "." + gamemode + ".level", (int)Math.sqrt(getPoints(player, gamemode)));
		else pd.getPlayerData().set(player.getUniqueId() + "." + gamemode + ".level", 1);
		pd.savePlayerData();
	}
	
	public static void addKill(Player player, String gamemode) {
		pd.getPlayerData().set(player.getUniqueId() + "." + gamemode + ".kills", getKills(player, gamemode) + 1);
		pd.savePlayerData();
	}
	
	public static void removePoints(Player player, int points, String gamemode) {
		pd.getPlayerData().set(player.getUniqueId() + "." + gamemode +".points", getPoints(player, gamemode) - points);
	    pd.savePlayerData();
	    setLevel(player, gamemode);
	}
	
	public static void setGamemode(Player player, String gamemode) {
		pd.getPlayerData().set(player.getUniqueId() + ".gamemode", gamemode);
		pd.savePlayerData();
		Scoreboard.setScoreboard(player, gamemode);
	}
	
	public static String getGamemode(Player player) {
		return pd.getPlayerData().getString(player.getUniqueId() + ".gamemode");
	}
	
}
