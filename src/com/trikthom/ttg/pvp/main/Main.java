package com.trikthom.ttg.pvp.main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.trikthom.ttg.pvp.data.Config;
import com.trikthom.ttg.pvp.data.EN;
import com.trikthom.ttg.pvp.data.NL;
import com.trikthom.ttg.pvp.data.PlayerData;
import com.trikthom.ttg.pvp.events.Chat;
import com.trikthom.ttg.pvp.events.Death;
import com.trikthom.ttg.pvp.events.Join;
import com.trikthom.ttg.pvp.events.Move;
import com.trikthom.ttg.pvp.events.Overall;
import com.trikthom.ttg.pvp.events.Quit;
import com.trikthom.ttg.pvp.events.VillagerInteract;
import com.trikthom.ttg.pvp.menus.Duel;

public class Main extends JavaPlugin implements Listener  {
	
	public static Plugin plugin;
	public static PlayerData pd = PlayerData.getInstance();
	public static NL nl = NL.getInstance();
	public static EN en = EN.getInstance();

	//public static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_WEST_3).build();
	//public static DynamoDB db = new DynamoDB(client);

	public void onEnable() {
		
		plugin = this;
		
		registerData();
		registerEvents();
	    registerCommands();
	    
	}
	
	public void registerData() {
		
		pd.setup(this);
		pd.getPlayerData().options().copyDefaults(true);
		pd.savePlayerData();
	    
	    nl.setup(this);
	    nl.getNLData().options().copyDefaults(true);
	    nl.saveNLData();
		
		en.setup(this);
	    en.getENData().options().copyDefaults(true);
	    en.saveENData();
	    
	    Config.registerDefaults();
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	}

	public void registerEvents() {
		
		Bukkit.getServer().getPluginManager().registerEvents(new Join(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Quit(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Death(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Chat(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Overall(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Move(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new VillagerInteract(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Duel(), this);
		
	}
	
	public void registerCommands() {
		
	}
	
	public static String line() {
		return ChatColor.RED + "" + ChatColor.STRIKETHROUGH + "----------------------------------------";
	}
	
}
