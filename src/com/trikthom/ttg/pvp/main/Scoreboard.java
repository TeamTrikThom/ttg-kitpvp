package com.trikthom.ttg.pvp.main;

import org.bukkit.entity.Player;

import com.trikthom.ttg.pvp.scoreboards.Duel;
import com.trikthom.ttg.pvp.scoreboards.DuelLobby;
import com.trikthom.ttg.pvp.scoreboards.Kit;

public class Scoreboard {

	public static void setScoreboard(Player player, String scoreboard) {
		if (scoreboard == "kit") Kit.setScoreboard(player);
		if (scoreboard == "duel") Duel.setScoreboard(player);
		if (scoreboard == "duel-lobby") DuelLobby.setScoreboard(player);
	}
	
}
