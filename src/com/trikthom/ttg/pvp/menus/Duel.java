package com.trikthom.ttg.pvp.menus;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Duel implements Listener {

	public static void openMainMenu(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "Duels");

		List<String> lore = new ArrayList<String>();
		lore.add("�7In Queue: �c" + 0);
	    ItemStack SG = new ItemStack(Material.FISHING_ROD);
	    ItemMeta CSG = SG.getItemMeta();
	    CSG.setDisplayName(ChatColor.GOLD + "�lRanked");
	    CSG.setLore(lore);
	    SG.setItemMeta(CSG);
	    
	    lore.clear();
	    lore.add("�7In Queue: �c" + 0);
	    ItemStack KOHI = new ItemStack(Material.GLASS_BOTTLE);
	    ItemMeta CKOHI = KOHI.getItemMeta();
	    CKOHI.setDisplayName("�lUnranked");
	    CKOHI.setLore(lore);
	    KOHI.setItemMeta(CKOHI);
	    
	    lore.clear();
	    lore.add("�7In Queue: �c" + 0);
	    ItemStack UHC = new ItemStack(Material.GOLDEN_APPLE);
	    ItemMeta CUHC = UHC.getItemMeta();
	    CUHC.setDisplayName(ChatColor.YELLOW + "�lPractice");
	    CUHC.setLore(lore);
	    UHC.setItemMeta(CUHC);
	    
	    inv.setItem(2, SG);
	    inv.setItem(4, KOHI);
	    inv.setItem(6, UHC);
		
	    player.openInventory(inv);
	}
	
	public static void openRankedDuelsMenu(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "Ranked Duels");

		List<String> lore = new ArrayList<String>();
		lore.add("�7In Queue: �c" + 0);
		lore.add("�7- �cIron Armour");
		lore.add("�7- �cIron Sword");
		lore.add("�7- �cFishing Rod");
		lore.add("�7- �cBow");
	    ItemStack SG = new ItemStack(Material.FISHING_ROD);
	    ItemMeta CSG = SG.getItemMeta();
	    CSG.setDisplayName("�7mode: " + ChatColor.GOLD + "�lSG");
	    CSG.setLore(lore);
	    SG.setItemMeta(CSG);
	    
	    lore.clear();
	    lore.add("�7In Queue: �c" + 0);
	    lore.add("�7- �cDiamond Armour");
		lore.add("�7- �cDiamond Sword");
		lore.add("�7- �c27x Instant Healt potion");
		lore.add("�7- �c4x Speed potion");
	    ItemStack KOHI = new ItemStack(Material.GLASS_BOTTLE);
	    ItemMeta CKOHI = KOHI.getItemMeta();
	    CKOHI.setDisplayName("�7mode: " + ChatColor.AQUA + "�lKOHI");
	    CKOHI.setLore(lore);
	    KOHI.setItemMeta(CKOHI);
	    
	    lore.clear();
	    lore.add("�7In Queue: �c" + 0);
	    lore.add("�7- �cDiamond Armour");
		lore.add("�7- �cDiamond Sword");
		lore.add("�7- �cUCH Items");
	    ItemStack UHC = new ItemStack(Material.GOLDEN_APPLE);
	    ItemMeta CUHC = UHC.getItemMeta();
	    CUHC.setDisplayName("�7mode: " + ChatColor.YELLOW + "�lBuild UHC");
	    CUHC.setLore(lore);
	    UHC.setItemMeta(CUHC);
	    
	    inv.setItem(0, SG);
	    inv.setItem(1, KOHI);
	    inv.setItem(2, UHC);
		
	    player.openInventory(inv);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
	  	Player player = (Player)event.getWhoClicked();
	  	
	  	if (!event.getInventory().getTitle().equals("Duels")) return;
	  	if (event.getCurrentItem() == null) return;
	  	switch (event.getCurrentItem().getType().getId()) {
		    case 346:
		    	openRankedDuelsMenu(player);
		    	break;
		    case 374:
		    	player.closeInventory();
		        break;
		    case 322:
		    	player.closeInventory();
		        break;
	    }
	  	event.setCancelled(true);
	  	
	}
	
}
